import pandas as pd
from rdflib import Graph, URIRef, Literal, Namespace, BNode
from rdflib.namespace import RDF, XSD, RDFS
import os
import sys
import time

# Define the namespaces
SAREF = Namespace("https://saref.etsi.org/core/")
EX = Namespace("http://example.org/")
OM = Namespace("http://www.ontology-of-units-of-measure.org/resource/om-2/")

# Function to determine if a value can be converted to a float
def is_float(value):
    try:
        float(value)
        return True
    except ValueError:
        return False
    
    
    
# Function to create RDF triples from CSV data
def create_rdf_triples(csv_file, output_file, limit=None):
    # Initialize RDF graph
    g = Graph()
    g.bind("saref", SAREF)
    g.bind("ex", EX)
    g.bind("om", OM)
    
     # Load the CSV file into a DataFrame
    df = pd.read_csv(csv_file)
    
    # Determine the range of rows to process
    if limit is not None:
        df = df.iloc[:limit]
    
    # Get the first two columns as timestamps
    timestamp1 = df.iloc[:, 0]
    timestamp2 = df.iloc[:, 1]
    
    # Skip the first two columns and interpolated markers
    data_columns = df.columns[2:-1]
    
    # Define the relatesToPropertyMarker
    daily_power_uri = EX.HourlyPower
    g.add((daily_power_uri, RDF.type, SAREF.Power))
    g.add((daily_power_uri, RDFS.label, Literal("Hourly Power")))

    watt_uri = OM.Watt
    g.add((watt_uri, RDF.type, SAREF.PowerUnit))
    g.add((watt_uri, RDFS.label, Literal("Watt")))
    
    # Iterate through each remaining column
    for column in data_columns:

        device_uri = URIRef(SAREF[column])       
        # Add triples for devices
        g.add((device_uri, RDF.type, SAREF['Device']))
    
        
        for idx, (ts1, ts2, data) in enumerate(zip(timestamp1, timestamp2, df[column])):
            if pd.notna(data):  # Check if the data is not missing (NaN)
                
                # Create a unique ID for the measurement
                measurement_id = f"{column}_Measurement{idx}"
                measurement_URI = URIRef(EX[measurement_id])  
                
                # Check the data type and add the appropriate Literal 
                # however we have skipped the final row with interpolated data 
                # so we dont have to worry anymore but still good I guess
                g.add((measurement_URI, RDF.type, SAREF['Measurement']))

                if is_float(data):
                    g.add((measurement_URI, SAREF['hasValue'], Literal(float(data), datatype=XSD.float)))
                else:
                    g.add((measurement_URI, SAREF['hasValue'], Literal(data, datatype=XSD.string)))
                
                
                g.add((measurement_URI, SAREF['hasTimestamp'], Literal(ts1, datatype=XSD.dateTime)))
                g.add((measurement_URI, SAREF['hasTimestamp'], Literal(ts2, datatype=XSD.dateTime)))
                g.add((measurement_URI, SAREF['isMeasuredIn'], watt_uri))
                g.add((measurement_URI, SAREF['relatesToProperty'], daily_power_uri))
                g.add((device_uri, SAREF['makesMeasurement'], measurement_URI))
                

    g.serialize(destination=output_file, format='turtle')

# Main function to handle script execution
def main():
    if len(sys.argv) != 2:
        print("Usage: python transformer.py path/to/dataset.csv")
        return
    
    csv_file = sys.argv[1]
    output_file = "graph.ttl"
    
    # Check if the file exists
    if not os.path.exists(csv_file):
        print(f"Error: The file {csv_file} does not exist.")
        return
    
    try:
        
        start_time = time.time()  # Record start time
        create_rdf_triples(csv_file, output_file, 100)
        end_time = time.time()  # Record end time
        execution_time = end_time - start_time

        print(f"RDF graph has been serialized to {output_file}")
        print(f"Execution time: {execution_time:.2f} seconds")

    except Exception as e:
        print(f"An error occurred: {e}")

if __name__ == "__main__":
    main()
